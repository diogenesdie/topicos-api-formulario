const express = require('express')
const router = express.Router()

router.all('/', (req, res) => {
    const method = req.method

    if( ['POST'].indexOf(method) == -1 ){
        return res.status(405).json({
            message: 'Method not allowed'
        })
    }

    const { name, email, birth, genre, cellphone, phone, smoker, city } = req.body

    console.log('Name: ', name)
    console.log('Email: ', email)
    console.log('Birth: ', birth)
    console.log('Genre: ', genre)
    console.log('Cellphone: ', cellphone)
    console.log('Phone: ', phone)
    console.log('Smoker: ', smoker)
    console.log('City: ', city)

    return res.status(200).json(req.body)
})

module.exports = router